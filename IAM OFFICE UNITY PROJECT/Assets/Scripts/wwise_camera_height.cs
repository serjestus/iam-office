using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wwise_camera_height : MonoBehaviour
{
    public GameObject camera;
    public AK.Wwise.Event OfficeAmbEvent;

    // Start is called before the first frame update
    void Start()
    {
        OfficeAmbEvent.Post(gameObject);
    }

    // Update is called once per frame 
    void Update()
    {
        AkSoundEngine.SetRTPCValue("ext_camera_height", camera.transform.position.y);

        

    }
     
}

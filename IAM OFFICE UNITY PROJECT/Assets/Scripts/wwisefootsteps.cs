using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class wwisefootsteps : MonoBehaviour
{
    public AK.Wwise.Event footstepsevent; // Переменная для хранения Wwise-ивента
    

    private vThirdPersonInput tpInput; // Переменная для обращения к магнитуде
    public LayerMask lm; // Выбор лейермаски (делается в компоненте)
    public GameObject leftfoot;
    public GameObject rightfoot;
    
    void Start()
    {
        tpInput = gameObject.GetComponent<vThirdPersonInput>(); // Делаем ссылку на компонент, который находится на контроллере
    }
    
    void footstep(string noga) // принимаем из Animation Event в функции строчную переменную right или left и передаем ее в переменную noga
    {
        if (tpInput.cc.inputMagnitude > 0.1) // Проверяем магнитуду
        {
            if (noga == "right") //если вызвали функцию с аргументом right
            {
                PlayShagi(rightfoot); // Запускаем функцию на объекте правой ноги 
            }
            else if (noga == "left") // то же самое для левой ноги
            {
                PlayShagi(leftfoot); // Запускаем функцию на объекте левой ноги
            }


        }
    }

   

    void PlayShagi(GameObject nogaObject) // функция проверки поверхности для нужного геймобъекта
    {
        if (Physics.Raycast(nogaObject.transform.position, Vector3.down, out RaycastHit hit, 0.3f, lm)) // Кидаем луч вниз и проверяем рейкастом поверхность
        { 
            //Debug.Log(hit.collider.tag); // В дебаге отображаем что за поверхность под ногами
            AkSoundEngine.SetSwitch("Surface_type", hit.collider.tag, nogaObject); // Выставляем свитч в свитчгруппе SWITCH_surface_type в положение поверхности,
            // котороая под ногами. На занятии была ошибка с тем, что скопировал свитч с пробелом и такие тэги поверхности с делал!
            footstepsevent.Post(nogaObject); // Запускаем ивент на геймобъекте nogaObject - левая ступня или правая
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
public class cloth_sound : MonoBehaviour
{

    public AK.Wwise.Event cloth_event;
    public vThirdPersonInput tpInput;


    // Start is called before the first frame update
    void Start()
    {
        tpInput = gameObject.GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void cloth()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            cloth_event.Post(gameObject);
        }
    }
}

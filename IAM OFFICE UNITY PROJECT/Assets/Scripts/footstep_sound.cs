using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using AK;

public class footstep_sound : MonoBehaviour
{


    public AK.Wwise.Event footstep_event;
    public vThirdPersonInput tpInput;
    public LayerMask lm;
    private string surface;


    // Start is called before the first frame update
    void Start()
    {
        tpInput = gameObject.GetComponent<vThirdPersonInput>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void footstep()
    {
        if (tpInput.cc.inputMagnitude > 0.1)

        {
            surfacecheck();
            AkSoundEngine.SetSwitch("Surface_type", surface, gameObject);
            footstep_event.Post(gameObject);

           

        }
    }

    void surfacecheck()
    {

        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            Debug.Log(hit.collider.tag);
            surface = hit.collider.tag;
        }

    }

    
}

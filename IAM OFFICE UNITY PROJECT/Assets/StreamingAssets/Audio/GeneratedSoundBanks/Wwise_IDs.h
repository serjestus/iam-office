/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AQUARIUM = 456876952U;
        static const AkUniqueID FOOTSTEP_WALK = 1161196797U;
        static const AkUniqueID PLAY_AQUARIUM = 2253830875U;
        static const AkUniqueID PLAY_CLOTHRANDOM = 1092290003U;
        static const AkUniqueID PLAY_COOLERBUBBLES = 468087869U;
        static const AkUniqueID PLAY_GAMEMACHINE = 622075691U;
        static const AkUniqueID PLAY_MOSCITOS = 1450198157U;
        static const AkUniqueID PLAY_OUTSIDE = 473294595U;
        static const AkUniqueID PLAY_ROOMTONE = 2748311587U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace INDOOR_OUTDOOR
        {
            static const AkUniqueID GROUP = 145721553U;

            namespace STATE
            {
                static const AkUniqueID INDOOR = 340398852U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OUTDOOR = 144697359U;
            } // namespace STATE
        } // namespace INDOOR_OUTDOOR

    } // namespace STATES

    namespace SWITCHES
    {
        namespace SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 4064446173U;

            namespace SWITCH
            {
                static const AkUniqueID CARPET = 2412606308U;
                static const AkUniqueID CONCRETE = 841620460U;
                static const AkUniqueID TILE = 2637588553U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID EXT_CAMERA_HEIGHT = 1862344870U;
        static const AkUniqueID GAMEMACHINEAZIMUTH = 796532562U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID INDOOR = 340398852U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID OUTDOOR = 144697359U;
        static const AkUniqueID PLAYER = 1069431850U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
